using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace FoodOrderer.Domain.SeedWork
{
    public interface IRepository<TEntity> where TEntity : IAggregateRoot
    {
        TEntity FindById(Guid id);
        
        TEntity FindOne(Expression<Func<TEntity, bool>> spec);
        
        void Add(TEntity entity);
        void Update(TEntity entity);
        void Remove(TEntity entity);


        IQueryable Find(Expression<Func<TEntity, bool>> spec);
    }
}